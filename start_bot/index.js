import * as R from 'ramda'
import puppeteer from 'puppeteer'
import * as util from 'util'

const sleep = util.promisify(setTimeout)

const takeSnapshot = (page, sock) => {
  const time = new Date().getTime()
  const ip = R.replace(/\./, '-', sock.ip)
  return page.screenshot({
    path: `/tmp/${ip}${time}.png`,
  })
}

const secondsToMilliseconds = R.multiply(1000)

const startBrowser = async (url, sock, yld) => {
  const browser = await puppeteer.launch({
    headless: process.env.NODE_ENV !== 'development',
    args:     [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      `--proxy-server=socks5://${sock.ip}:${sock.port}`,
    ],
    executablePath: 'google-chrome-stable',
  })
  const page = await browser.newPage()
  await page.goto(url)

  await yld(page)

  await browser.close()
}

// /////////////////////////

const url = 'https://www.twitch.tv/evilvovan'

const socks5 = [
  { ip: '1.180.131.174', port: 1080 },
  { ip: '99.192.179.146', port: 1080 },
  { ip: '73.245.12.41', port: 11163 },
  { ip: '62.16.226.28', port: 10200 },
  { ip: '122.192.32.74', port: 7280 },
  // { ip: '114.33.75.32', port: 1080 },
  // { ip: '178.168.43.131', port: 1080 },
  // { ip: '181.129.40.42', port: 33555 },
  // { ip: '176.192.78.22', port: 1080 },
  // { ip: '54.250.204.219', port: 1080 },
]
;(async () => {
  const promises = socks5.map(sock =>
    startBrowser(url, sock, async page => {
      takeSnapshot(page, sock)
      await sleep(secondsToMilliseconds(100))
    })
  )

  await Promise.all(promises)
})()
