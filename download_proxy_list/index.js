import puppeteer from 'puppeteer'

const url = 'https://www.socks-proxy.net'
;(async () => {
  console.log(process.env.NODE_ENV === 'development')
  const browser = await puppeteer.launch({
    headless:       process.env.NODE_ENV !== 'development',
    args:           ['--no-sandbox', '--disable-setuid-sandbox'],
    executablePath: 'google-chrome-stable',
  })
  const page = await browser.newPage()
  await page.goto(url)
  const getTableData = async () =>
    page.$$eval('.dataTable tr[class]', trs =>
      Array.from(trs.children).map(x => x.innerText)
    )

  const tableData = await getTableData()
  console.log(tableData)

  debugger

  await browser.close()
})().catch(console.error)
